import wkhtmltopdf from "wkhtmltopdf"
import { readFile} from 'fs/promises'

export const create = async (req, res) => {
  const { html } = req.body
  const filePath = 'uploads/file.pdf'
  wkhtmltopdf(html, { output: filePath })
  const file = await readFile(filePath)
  res.contentType("application/pdf")
  return res.status(200).send(file)
}
