import express from 'express'
import { create } from './controllers.js'

const routes = express.Router()

routes.post('/create', create)

export default routes
